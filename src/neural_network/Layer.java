package neural_network;

import java.io.Serializable;

/**
 * Created by Wald_Laptop on 8/17/2017.
 */
public class Layer implements Serializable {

    private Neuron[] neurons;

    public Layer(final Neuron[] neurons) {
        this.neurons = neurons;
    }

    public double[] getActivationFunctionValues() {
        final double[] activationValues = new double[neurons.length];
        for (int a = 0; a < activationValues.length; a++) {
            activationValues[a] = neurons[a].calculateActivationFunctionValue();
        }
        return activationValues;
    }

    public Neuron[] getNeurons() {
        return neurons;
    }
}
