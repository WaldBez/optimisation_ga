package neural_network;

import java.io.Serializable;

/**
 * Created by Wald_Laptop on 8/16/2017.
 */
public class Neuron implements Serializable {

    private double[] weights;
    private double[] inputs;

    public Neuron(final int numberOfInputs) {
        initializeWeights(numberOfInputs);
    }

    public Neuron(final double[] weights) {
        this.weights = weights;
    }

    public void initializeWeights(final int numberOfInputs) {
        weights = new double[numberOfInputs];
        for (int a = 0; a < numberOfInputs; a++) {
            weights[a] = Math.random();
        }
    }

    public void setInputs(final double[] inputs) {
        this.inputs = inputs;
    }

    public double calculateActivationFunctionValue() {
        double value = 0;
        for (int a = 0; a < inputs.length; a++) {
            value = value + (inputs[a] * weights[a]);
        }
        return value;
    }
}
