package neural_network;

import domain.MoveEnum;

import java.util.HashMap;

/**
 * Created by Wald_Laptop on 8/18/2017.
 */
public class NetworkMapper {

    private HashMap<MoveEnum, Double> movesMap = new HashMap();

    public void mapAllMoves(final double[] networkOutput) {
        for (int a = 0; a < networkOutput.length; a++) {
            switch (a) {
                case 0:
                    movesMap.put(MoveEnum.MOVE_UP_AND_LEFT, new Double(networkOutput[a]));
                case 1:
                    movesMap.put(MoveEnum.MOVE_UP_AND_RIGHT, new Double(networkOutput[a]));
                case 2:
                    movesMap.put(MoveEnum.MOVE_DOWN_AND_LEFT, new Double(networkOutput[a]));
                case 3:
                    movesMap.put(MoveEnum.MOVE_DOWN_AND_RIGHT, new Double(networkOutput[a]));
            }

        }
    }

    public HashMap<MoveEnum, Double> getMovesMap() {
        return movesMap;
    }
}
