package neural_network;

import java.io.Serializable;

/**
 * Created by Wald_Laptop on 8/17/2017.
 */
public class NeuralNetwork implements Serializable {

    private static final int NUMBER_OF_LAYERS = 2;
    private Layer[] network;

    public NeuralNetwork(final Layer[] layers) {
        network = new Layer[NUMBER_OF_LAYERS];
        for (int a = 0; a < NUMBER_OF_LAYERS; a++) {
            network[a] = layers[a];
        }
    }

    public void setNetworkWeightsLayers(final Layer[] layers) {
        network = layers;
    }

    public double[] runInNetwork(final double[] inputs, final int currentLayer) {
        if (currentLayer == NUMBER_OF_LAYERS)
            return inputs;
        final double[] localInput = new double[network[currentLayer].getNeurons().length];
        for (int a = 0; a < network[currentLayer].getNeurons().length; a++) {
            network[currentLayer].getNeurons()[a].setInputs(inputs);
            localInput[a] = network[currentLayer].getNeurons()[a].calculateActivationFunctionValue();
        }
        return runInNetwork(localInput, currentLayer + 1);
    }
}
