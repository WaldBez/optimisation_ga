package persistance;

import domain.Animal;
import util.Constants;

import java.io.*;

/**
 * Created by Wald_Laptop on 8/30/2017.
 */
public class AnimalFIleHandler {

    public void saveAnimalsToFile(final Animal[] animals) {
        try {
            final FileOutputStream fos = new FileOutputStream(Constants.ANIMAL_FILE_LOCATION);
            final ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(animals);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Animal[] loadAnimalsFromFile() {
        try {
            final FileInputStream fis = new FileInputStream(Constants.ANIMAL_FILE_LOCATION);
            final ObjectInputStream ois = new ObjectInputStream(fis);
            return (Animal[]) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("File not yet found.");
        }
        return null;
    }
}
