package persistance;

import util.Constants;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Wald_Laptop on 8/30/2017.
 */
public class LogStatisticsToFile {

    public void saveStatisticsToFile(final String text) {
        try {
            final PrintWriter pw = new PrintWriter(new FileOutputStream(
                    new File(Constants.STATISTICS_FILE_LOCATION), true));
            pw.append(text);
            pw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
