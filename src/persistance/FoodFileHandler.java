package persistance;

import domain.Food;
import util.Constants;

import java.io.*;

/**
 * Created by Wald_Laptop on 8/30/2017.
 */
public class FoodFileHandler {

    public void saveFoodToFile(final Food[] food) {
        try {
            final FileOutputStream fos = new FileOutputStream(Constants.FOOD_FILE_LOCATION);
            final ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(food);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Food[] loadFoodFromFile() {
        try {
            final FileInputStream fis = new FileInputStream(Constants.FOOD_FILE_LOCATION);
            final ObjectInputStream ois = new ObjectInputStream(fis);
            return (Food[]) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("File not yet found.");
        }
        return null;
    }
}
