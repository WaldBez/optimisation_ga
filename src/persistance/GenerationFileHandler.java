package persistance;

import domain.Generation;
import util.Constants;

import java.io.*;

/**
 * Created by Wald_Laptop on 8/30/2017.
 */
public class GenerationFileHandler {

    public void saveGenerationToFile(final Generation generation) {
        try {
            final FileOutputStream fos = new FileOutputStream(Constants.GENERATION_FILE_LOCATION);
            final ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(generation);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Generation loadGenerationFromFile() {
        try {
            final FileInputStream fis = new FileInputStream(Constants.GENERATION_FILE_LOCATION);
            final ObjectInputStream ois = new ObjectInputStream(fis);
            return (Generation) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("File not yet found.");
        }
        return null;
    }

}
