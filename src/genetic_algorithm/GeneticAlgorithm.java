package genetic_algorithm;

import domain.Animal;
import neural_network.Layer;
import neural_network.NeuralNetwork;
import neural_network.Neuron;
import util.Constants;

/**
 * Created by Wald_Laptop on 8/18/2017.
 */
public class GeneticAlgorithm {

    private Animal[] population;

    public Animal[] initializePopulation() {
        population = new Animal[Constants.POPULATION_SIZE];
        for (int a = 0; a < population.length; a++) {
            double[][][] weights = {{{Math.random(), Math.random(), Math.random(), Math.random()},
                    {Math.random(), Math.random(), Math.random(), Math.random()}},
                    {{Math.random(), Math.random()}, {Math.random(), Math.random()},
                            {Math.random(), Math.random()}, {Math.random(), Math.random()}}};
            population[a] = new Animal(initializeAnimalBrain(weights), weights, 0);
        }
        return population;
    }

    public Animal createChild(final Animal[] animals, final int generation) {
        final Animal[] parents = selectParents(tournamentSelectedParents(animals), new Animal[2], 0);
        Animal child = crossover(parents[0], parents[1], generation);
        mutateAnimal(child);
        return child;
    }

    private Animal[] tournamentSelectedParents(final Animal[] animals) {
        Animal[] tournamentAnimals = new Animal[Constants.TOURNAMENT_SIZE];
        for (int a = 0; a < tournamentAnimals.length; a++) {
            int random = (int)(Math.random() * animals.length);
            tournamentAnimals[a] = animals[random];
        }
        return tournamentAnimals;
    }

    private Animal crossover(final Animal parent1, final Animal parent2, final int generation) {
        double[][][] weights = new double[parent1.getWeights().length][][];
        for (int a = 0; a < parent1.getWeights().length; a++) {
            weights[a] = new double[parent1.getWeights()[a].length][];
            for (int b = 0; b < parent1.getWeights()[a].length; b++) {
                weights[a][b] = new double[parent1.getWeights()[a][b].length];
                for (int c = 0; c < weights[a][b].length; c++) {
                    if (c % 2 == 0) {
                        weights[a][b][c] = parent1.getWeights()[a][b][c];
                    } else {
                        weights[a][b][c] = parent2.getWeights()[a][b][c];
                    }
                }
            }
        }
        return new Animal(initializeAnimalBrain(weights), weights, generation);
    }

    private void mutateAnimal(final Animal animal) {
        for (int a = 0; a < animal.getWeights().length; a++) {
            for (int b = 0; b < animal.getWeights()[a].length; b++) {
                for (int c = 0; c < animal.getWeights()[a][b].length; c++) {
                    if (Math.random() <= Constants.MUTATION_RATE) {
                        animal.getWeights()[a][b][c] = Math.random();
                    }
                }
            }
        }
    }

    private Animal[] selectParents(final Animal[] animals, final Animal[] parents, final int currentParent) {
        if (parents[1] != null) {
            return parents;
        }
        parents[currentParent] = animals[0];
        for (final Animal animal : animals) {
            if (calculateFitnessOfAnimal(parents[currentParent]) < calculateFitnessOfAnimal(animal)) {
                if (currentParent == 1) {
                    if (parents[0] != animal) {
                        parents[currentParent] = animal;
                    }
                } else {
                    parents[currentParent] = animal;
                }
            }
        }
        return selectParents(animals, parents, currentParent + 1);
    }

    private int calculateFitnessOfAnimal(final Animal animal) {
        return animal.getTimeAlive() - applyFitnessPenalty(animal) + applyFitnessAdvantage(animal);
    }

    private int applyFitnessPenalty(final Animal animal) {
        if (animal.getX() <= 0 || animal.getY() <= 0
                || animal.getX() >= Constants.MAP_WIDTH - Constants.MAP_WIDTH_OFFSET
                || animal.getY() >= Constants.MAP_HEIGHT - Constants.MAP_HEIGHT_OFFSET)
            return Constants.FITNESS_PENALTY;
        return 0;
    }

    private int applyFitnessAdvantage(final Animal animal) {
        return animal.getAmountOfFoodEaten() + Constants.FITNESS_ADVANTAGE;
    }

    private NeuralNetwork initializeAnimalBrain(final double[][][] weightsForLayers) {
        return new NeuralNetwork(initializeNeuralNetworkLayers(weightsForLayers));
    }

    private Layer[] initializeNeuralNetworkLayers(final double[][][] weightsForLayers) {
        Layer[] layers = new Layer[weightsForLayers.length];
        for (int a = 0; a < weightsForLayers.length; a++) {
            layers[a] = initializeLayer(weightsForLayers[a]);
        }
        return layers;
    }

    private Layer initializeLayer(final double[][] weights) {
        Neuron[] neurons = new Neuron[weights.length];
        for (int a = 0; a < weights.length; a++) {
            double[] weightsPerNeuron = new double[weights[a].length];
            for (int b = 0; b < weights[a].length; b++) {
                weightsPerNeuron[b] = weights[a][b];
            }
            neurons[a] = new Neuron(weightsPerNeuron);
        }
        return new Layer(neurons);
    }
}
