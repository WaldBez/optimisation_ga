package domain;

import neural_network.NetworkMapper;
import neural_network.NeuralNetwork;
import util.Constants;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;

/**
 * Created by Wald_Laptop on 8/18/2017.
 */
public class Animal implements Serializable {

    private int x;
    private int y;
    private int generation;
    private int health;
    private int amountOfFoodEaten = 0;
    private int timeAlive = 0;
    private boolean alive = true;
    private NeuralNetwork brain;
    private HashMap<MoveEnum, Double> moves;
    private double[][][] weights;

    public Animal(final NeuralNetwork brain, final double[][][] weights, final int generation) {
        final Random random = new Random();
        x = random.nextInt(Constants.MAP_WIDTH - Constants.MAP_WIDTH_OFFSET);
        y = random.nextInt(Constants.MAP_HEIGHT - Constants.MAP_HEIGHT_OFFSET);
        health = Constants.MAX_HEALTH;
        this.brain = brain;
        this.weights = weights;
        this.generation = generation;
    }

    public void liveLife(final Food[] food) {
        if (alive) {
            move(food);
            decreaseHealth();
            eatFood(food);
            System.out.println(toString());
        }
    }

    public void eatFood(final Food[] food) {
        for (final Food f : food) {
            if ((f.getX() >= x - 4 && f.getX() <= x + 4) &&
                    (f.getY() >= y - 4 && f.getY() <= y + 4)) {
                health += f.getAmountOfHealth();
                amountOfFoodEaten = amountOfFoodEaten + 1;
                f.initializeFood();
            }
        }
    }

    public void move(final Food[] food) {
        final NetworkMapper mapper = new NetworkMapper();
        mapper.mapAllMoves(brain.runInNetwork(closesFoodPerQuadrant(food), 0));
        moves = mapper.getMovesMap();
        makeMove();
    }

    public void draw (final Graphics g) {
        if (alive) {
            g.setColor(Color.WHITE);
            try {
                g.drawImage(ImageIO.read(new File(Constants.ANIMAL_IMAGE)), x, y, 10, 10, null);
            } catch (IOException e) {
                e.printStackTrace();
            }
            g.drawString("TimeAlive:" + timeAlive, x - 20, y - 15);
            g.drawString("Generation: " + generation, x - 20, y - 5);
        }
    }

    public void decreaseHealth() {
        if (health == 0)
            alive = false;
        health--;
        timeAlive++;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public double[][][] getWeights() {
        return weights;
    }

    public int getTimeAlive() {
        return timeAlive;
    }

    public boolean isAlive() {
        return alive;
    }

    public int getAmountOfFoodEaten() {
        return amountOfFoodEaten;
    }

    public String fetchStatistics() {
        return generation + ", " + this.timeAlive + ", " + this.amountOfFoodEaten + ", " + weightsAsText() + "\n";
    }

    private String weightsAsText() {
        String weightsString = "";
        for (final double[][] d1 : weights) {
            weightsString +=  "{";
            for (final double[] d2 : d1) {
                weightsString +=  "{";
                for (int a = 0; a < d2.length; a++) {
                    if (a == d2.length - 1)
                        weightsString += d2[a];
                    else
                        weightsString += d2[a] + ", ";
                }
                weightsString +=  "}";
            }
            weightsString +=  "}";
        }
        return weightsString;
    }

    @Override
    public String toString() {
        return "Animal{" +
                "x=" + x +
                ", y=" + y +
                ", health=" + health +
                ", timeAlive=" + timeAlive +
                ", isAlive=" + alive +
                ", brain=" + brain +
                ", moves=" +  +
                '}';
    }

    private void makeMove() {
        final Iterator moveIterator = moves.entrySet().iterator();
        Map.Entry bestMove = (Map.Entry) moveIterator.next();
        while (moveIterator.hasNext()) {
            Map.Entry move = (Map.Entry) moveIterator.next();
            if ((double)move.getValue() < (double)bestMove.getValue()) {
                bestMove = move;
            }
        }
        if (bestMove.getKey().equals(MoveEnum.MOVE_UP_AND_LEFT)) {
            if (x >= 0)
                x--;
            if (y >= 0)
                y--;
        } else if (bestMove.getKey().equals(MoveEnum.MOVE_UP_AND_RIGHT)) {
            if (x <= Constants.MAP_WIDTH - Constants.MAP_WIDTH_OFFSET)
                x++;
            if (y >= 0)
                y--;
        } else if (bestMove.getKey().equals(MoveEnum.MOVE_DOWN_AND_LEFT)) {
            if (x >= 0)
                x--;
            if (y <= Constants.MAP_HEIGHT - Constants.MAP_HEIGHT_OFFSET)
                y++;
        } if (bestMove.getKey().equals(MoveEnum.MOVE_DOWN_AND_RIGHT)) {
            if (x <= Constants.MAP_WIDTH - Constants.MAP_WIDTH_OFFSET)
                x++;
            if (y <= Constants.MAP_HEIGHT - Constants.MAP_HEIGHT_OFFSET)
                y++;
        }
    }

    private double[] closesFoodPerQuadrant(final Food[] food) {
        final double[] inputs = new double[Constants.NUMBER_OF_SENSORS];
        final Food[] foodInUpperLeftQuadrant = findFoodInUpperLeftQuadrant(food);
        final Food[] foodInUpperRightQuadrant = findFoodInUpperRightQuadrant(food);
        final Food[] foodInLowerLeftQuadrant = findFoodInLowerLeftQuadrant(food);
        final Food[] foodInLowerRightQuadrant = findFoodInLowerRightQuadrant(food);
        inputs[0] = findClosestFoodInQuadrant(foodInUpperLeftQuadrant);
        inputs[1] = findClosestFoodInQuadrant(foodInUpperRightQuadrant);
        inputs[2] = findClosestFoodInQuadrant(foodInLowerLeftQuadrant);
        inputs[3] = findClosestFoodInQuadrant(foodInLowerRightQuadrant);
        return inputs;
    }

    private Food[] findFoodInUpperLeftQuadrant(final Food[] food) {
        final ArrayList<Food> foodInQuadrant = new ArrayList<>();
        for (final Food f : food) {
            if (f.getX() <= x && f.getY() <= y) {
                foodInQuadrant.add(f);
            }
        }
        return foodInQuadrant.toArray(new Food[foodInQuadrant.size()]);
    }

    private Food[] findFoodInUpperRightQuadrant(final Food[] food) {
        final ArrayList<Food> foodInQuadrant = new ArrayList<>();
        for (final Food f : food) {
            if (f.getX() >= x && f.getY() <= y) {
                foodInQuadrant.add(f);
            }
        }
        return foodInQuadrant.toArray(new Food[foodInQuadrant.size()]);
    }

    private Food[] findFoodInLowerLeftQuadrant(final Food[] food) {
        final ArrayList<Food> foodInQuadrant = new ArrayList<>();
        for (final Food f : food) {
            if (f.getX() <= x && f.getY() >= y) {
                foodInQuadrant.add(f);
            }
        }
        return foodInQuadrant.toArray(new Food[foodInQuadrant.size()]);
    }

    private Food[] findFoodInLowerRightQuadrant(final Food[] food) {
        final ArrayList<Food> foodInQuadrant = new ArrayList<>();
        for (final Food f : food) {
            if (f.getX() >= x && f.getY() >= y) {
                foodInQuadrant.add(f);
            }
        }
        return foodInQuadrant.toArray(new Food[foodInQuadrant.size()]);
    }

    private double findClosestFoodInQuadrant(final Food[] food) {
        if (food.length > 0) {
            double closesFood = Math.sqrt((Math.abs(food[0].getX() - x) * Math.abs(food[0].getX() - x))
                    + (Math.abs(food[0].getY() - y) * Math.abs(food[0].getY() - y)));
            for (final Food f : food) {
                final double distance = Math.sqrt((Math.abs(f.getX() - x) * Math.abs(f.getX() - x))
                        + (Math.abs(f.getY() - y) * Math.abs(f.getY() - y)));
                if (closesFood > distance) {
                    closesFood = distance;
                }
            }
            return closesFood;
        }
        return 10000000;
    }
}
