package domain;

/**
 * Created by Wald_Laptop on 8/18/2017.
 */
public enum MoveEnum {
    MOVE_UP_AND_LEFT(0),
    MOVE_UP_AND_RIGHT(1),
    MOVE_DOWN_AND_LEFT(2),
    MOVE_DOWN_AND_RIGHT(3);

    private final int move;

    MoveEnum(final int move) {
        this.move = move;
    }

    public int getValue() { return move; }
}
