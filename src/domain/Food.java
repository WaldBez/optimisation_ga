package domain;

import util.Constants;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.Random;

/**
 * Created by Wald_Laptop on 8/18/2017.
 */
public class Food implements Serializable {

    private int x;
    private int y;
    private int amountOfHealth = 0;
    private boolean hasBeenEaten = false;

    public Food() {
        initializeFood();
    }

    public void draw (final Graphics g) {
        try {
            g.drawImage(ImageIO.read(new File(Constants.FOOD_IMAGE)), x, y, 10, 10, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getAmountOfHealth() {
        return amountOfHealth;
    }

    public void setAmountOfHealth(int amountOfHealth) {
        this.amountOfHealth = amountOfHealth;
    }

    public void initializeFood() {
        final Random random = new Random();
        x = random.nextInt(Constants.MAP_WIDTH - (3 * Constants.MAP_WIDTH_OFFSET));
        y = random.nextInt(Constants.MAP_HEIGHT - (3 * Constants.MAP_HEIGHT_OFFSET));
        amountOfHealth = random.nextInt(Constants.MAX_FOOD_HEALTH);
    }
}
