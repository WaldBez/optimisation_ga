package domain;

import java.io.Serializable;

/**
 * Created by Wald_Laptop on 8/30/2017.
 */
public class Generation implements Serializable {

    private int generation;

    public Generation(final int generation) {
        this.generation = generation;
    }

    public int getGeneration() {
        return generation;
    }
}
