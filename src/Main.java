import neural_network.Layer;
import neural_network.NeuralNetwork;
import neural_network.Neuron;
import ui.UIWindow;
import util.Constants;

import javax.swing.*;

public class Main {

    public static void main(String[] args) {
//        Neuron[] neurons = new Neuron[2];
//        double[] tmp = {0.5, 0.4};
//        double[] tmp2 = {0.6, 0.1};
//        neurons[0] = new Neuron(tmp);
//        neurons[1] = new Neuron(tmp2);
//        Neuron[] neurons2 = new Neuron[4];
//        double[] tmp3 = {0.1, 0.2};
//        double[] tmp4 = {0.2, 0.1};
//        double[] tmp5 = {0.2, 0.2};
//        double[] tmp6 = {0.23, 0.44};
//        neurons2[0] = new Neuron(tmp3);
//        neurons2[1] = new Neuron(tmp4);
//        neurons2[2] = new Neuron(tmp5);
//        neurons2[4] = new Neuron(tmp6);
//        Layer[] layer = new Layer[2];
//        layer[0] = new Layer(neurons);
//        layer[1] = new Layer(neurons2);
//        NeuralNetwork neuralNetwork = new NeuralNetwork(layer);
//        double[] in = {2, 4};
//        double[] d = neuralNetwork.runInNetwork(in, 0);
//        System.out.println("Hello World!");
        UIWindow window = new UIWindow();
        window.setTitle("GA Simulation");
        window.setSize(Constants.MAP_WIDTH, Constants.MAP_HEIGHT);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setResizable(false);
        window.setLocationRelativeTo(null);
        window.setVisible(true);
    }
}
