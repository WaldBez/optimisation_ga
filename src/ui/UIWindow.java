package ui;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Wald_Laptop on 8/18/2017.
 */
public class UIWindow extends JFrame {

    private UIPanel panel;

    public UIWindow() {
        setLayout(new GridLayout(1, 1));
        panel = new UIPanel();
        add(panel);
    }
}
