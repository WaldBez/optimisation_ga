package ui;

import domain.Animal;
import domain.Food;
import domain.Generation;
import genetic_algorithm.GeneticAlgorithm;
import persistance.AnimalFIleHandler;
import persistance.FoodFileHandler;
import persistance.GenerationFileHandler;
import persistance.LogStatisticsToFile;
import util.Constants;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.Random;

/**
 * Created by Wald_Laptop on 8/18/2017.
 */
public class UIPanel extends JPanel implements Runnable{

    private GeneticAlgorithm geneticAlgorithm           = null;
    private boolean running                             = false;
    private Thread worldThread                          = null;
    private Food[] food                                 = null;
    private Animal[] animals                            = null;
    private boolean saveAnimals                         = false;
    private AnimalFIleHandler fileHandler               = null;
    private FoodFileHandler foodFileHandler             = null;
    private GenerationFileHandler generationFileHandler = null;
    private LogStatisticsToFile logStatisticsToFile     = null;
    private Generation generation                       = null;
    private int runtime                                 =    0;
    private Image background                            = null;

    public UIPanel() {
        start();
    }

    @Override
    protected void paintComponent(final Graphics g) {
        super.paintComponent(g);
        g.drawImage(background, 0, 0, Constants.MAP_WIDTH, Constants.MAP_HEIGHT, null);
        for (final Food f : food) {
            f.draw(g);
        }
        if (!Constants.TRAINING_MODE) {
            for (final Animal animal : animals) {
                animal.draw(g);
            }
        }
    }

    @Override
    public void run() {
        while (running) {
            for (int a = 0; a < animals.length; a++) {
                if (!animals[a].isAlive()) {
                    logStatisticsToFile.saveStatisticsToFile(runtime + ", " + animals[a].fetchStatistics());
                    animals[a] = geneticAlgorithm.createChild(animals, generation.getGeneration());
                    saveAnimals = true;
                }
                animals[a].liveLife(food);
            }
            if (saveAnimals) {
                generation = new Generation(generation.getGeneration() + 1);
                fileHandler.saveAnimalsToFile(animals);
                foodFileHandler.saveFoodToFile(food);
                generationFileHandler.saveGenerationToFile(generation);
                saveAnimals = false;
            }
            runtime = runtime + 1;
            repaint();
            if (!Constants.TRAINING_MODE) {
                try {
                    Thread.sleep(1000 / Constants.FRAME_RATE);
                } catch (final InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void start() {
        if (running) {
            return;
        }
        geneticAlgorithm = new GeneticAlgorithm();
        logStatisticsToFile = new LogStatisticsToFile();
        initializeFood();
        initializeGeneration();
        initializeAnimals();
        initializeImage();
        running = true;
        worldThread = new Thread(this);
        worldThread.start();
    }

    private void initializeImage() {
        try {
            background = ImageIO.read(new File(Constants.GRASS_IMAGE));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initializeGeneration() {
        generationFileHandler = new GenerationFileHandler();
        generation = generationFileHandler.loadGenerationFromFile();
        if (generation == null) {
            generation = new Generation(1);
        }
    }

    private void initializeAnimals() {
        fileHandler = new AnimalFIleHandler();
        animals = fileHandler.loadAnimalsFromFile();
        if (animals == null) {
            animals = geneticAlgorithm.initializePopulation();
        }
    }

    private void initializeFood() {
        foodFileHandler = new FoodFileHandler();
        food = foodFileHandler.loadFoodFromFile();
        if (food == null) {
            food = new Food[Constants.MAX_FOOD];
            for (int a = 0; a < food.length; a++) {
                food[a] = new Food();
            }
        }
    }
}
