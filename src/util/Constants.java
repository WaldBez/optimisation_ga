package util;

/**
 * Created by Wald_Laptop on 8/18/2017.
 */
public class Constants {
    public static int MAP_WIDTH                     =                     800;
    public static int MAP_HEIGHT                    =                     600;
    public static int MAP_WIDTH_OFFSET              =                      11;
    public static int MAP_HEIGHT_OFFSET             =                      35;
    public static int MAX_FOOD                      =                      20;
    public static int MAX_FOOD_HEALTH               =                    1000;
    public static int MAX_HEALTH                    =                     500;
    public static int POPULATION_SIZE               =                      20;
    public static int TOURNAMENT_SIZE               =                      20;
    public static int NUMBER_OF_SENSORS             =                       4;
    public static int FITNESS_PENALTY               =                     300;
    public static int FITNESS_ADVANTAGE             =                     100;
    public static int FRAME_RATE                    =                     240;
    public static double MUTATION_RATE              =                    0.15;
    public static boolean TRAINING_MODE             =                    false;
    public static String ANIMAL_FILE_LOCATION       =           "animals.txt";
    public static String FOOD_FILE_LOCATION         =              "food.txt";
    public static String GENERATION_FILE_LOCATION   =        "generation.txt";
    public static String STATISTICS_FILE_LOCATION   =        "statistics.csv";
    public static String ANIMAL_IMAGE               =     "resources/dog.png";
    public static String FOOD_IMAGE                 =    "resources/food.png";
    public static String GRASS_IMAGE                =   "resources/grass.png";
}
